Initiation à la programmation avec Python

Durée : 2 jours

pré-requis : pratique de l'utilisation d'un ordinateur

- Système, programmes et langages
  - Architecture des ordinateurs et systèmes d'exploitation
  - Qu'est-ce qu'un langage de programmation ?
  - Algorithmes
  - Paradigmes des langages de programmations
  - Compilateurs et interpréteurs
  - Exemples en divers langages
  - Notre premier programme

- Accès et traitement des données 
  - Noms et références
  - Types de données
  - Opérateurs, fonctions et méthodes
  - Booléens et alternatives

- Données composites 
  - Collections de données
  - Listes
  - Modification et extraction d'informations
  - Autres collections 

- Boucles
  - Parcours de listes
  - Parcours des autres collections
  - Compréhensions

- Fonctions
  - Structuration du code avec des fonctions
  - Passage de paramètres 
  - Renvoi de valeur
  - Appels de fonction

- Maintenance, débogage et tests
  - Interpréter les messages d'erreur
  - Utilisation d'un débogueur pas à pas
  - Test unitaires

- Programmation Orientée Objet
  - Classes et instances
  - Attributs et méthodes
  - Héritage
  - Méthodes spéciales

