# Plans des supports de cours

## Python programmation objet

Disponible en : fr, en

https://gitlab.com/python_431/plans-de-cours/-/blob/main/python-objets/plan-fr.txt

## Introduction à la programmation en Python

Disponible en : fr, en

https://gitlab.com/python_431/plans-de-cours/-/blob/main/intro-prog-python/plan-fr.txt
